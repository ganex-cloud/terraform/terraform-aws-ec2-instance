######
# Note: Create ec2 instance with custom network_interface
######
resource "aws_instance" "this" {
  count                                = "${var.associate_public_ip_address == "false" ? 1 : 0}"
  ami                                  = "${var.ami}"
  instance_type                        = "${var.instance_type}"
  user_data                            = "${var.user_data}"
  key_name                             = "${var.key_name}"
  monitoring                           = "${var.monitoring}"
  iam_instance_profile                 = "${var.iam_instance_profile}"
  ebs_optimized                        = "${var.ebs_optimized}"
  root_block_device                    = "${var.root_block_device}"
  ebs_block_device                     = "${var.ebs_block_device}"
  ephemeral_block_device               = "${var.ephemeral_block_device}"
  disable_api_termination              = "${var.disable_api_termination}"
  instance_initiated_shutdown_behavior = "${var.instance_initiated_shutdown_behavior}"
  placement_group                      = "${var.placement_group}"
  tenancy                              = "${var.tenancy}"

  network_interface {
    network_interface_id = "${aws_network_interface.this.id}"
    device_index         = 0
  }

  credit_specification {
    cpu_credits = "${var.cpu_credits}"
  }

  tags        = "${merge(map("Name", var.name), var.tags)}"
  volume_tags = "${merge(map("Name", var.name), var.volume_tags)}"

  lifecycle {
    ignore_changes = ["private_ip", "root_block_device", "ebs_block_device"]
  }
}

resource "aws_network_interface" "this" {
  count             = "${var.associate_public_ip_address == "false" ? 1 : 0}"
  subnet_id         = "${element(distinct(compact(concat(list(var.subnet_id), var.subnet_ids))),count.index)}"
  security_groups   = ["${var.vpc_security_group_ids}"]
  private_ip        = "${var.private_ip}"
  source_dest_check = "${var.source_dest_check}"
  tags              = "${merge(map("Name", var.name), var.tags)}"
}

resource "aws_eip" "this" {
  count             = "${var.associate_public_ip_address == "false" ? 1 : 0}"
  vpc               = "true"
  network_interface = "${aws_network_interface.this.id}"
  depends_on        = ["aws_instance.this"]
}

######
# Note: Create ec2 instance without custom network_interface
######
resource "aws_instance" "this2" {
  count                                = "${var.associate_public_ip_address == "true" ? 1 : 0}"
  ami                                  = "${var.ami}"
  instance_type                        = "${var.instance_type}"
  user_data                            = "${var.user_data}"
  subnet_id                            = "${element(distinct(compact(concat(list(var.subnet_id), var.subnet_ids))),count.index)}"
  key_name                             = "${var.key_name}"
  monitoring                           = "${var.monitoring}"
  vpc_security_group_ids               = ["${var.vpc_security_group_ids}"]
  iam_instance_profile                 = "${var.iam_instance_profile}"
  private_ip                           = "${var.private_ip}"
  associate_public_ip_address          = "${var.associate_public_ip_address}"
  ebs_optimized                        = "${var.ebs_optimized}"
  root_block_device                    = "${var.root_block_device}"
  ebs_block_device                     = "${var.ebs_block_device}"
  ephemeral_block_device               = "${var.ephemeral_block_device}"
  source_dest_check                    = "${var.source_dest_check}"
  disable_api_termination              = "${var.disable_api_termination}"
  instance_initiated_shutdown_behavior = "${var.instance_initiated_shutdown_behavior}"
  placement_group                      = "${var.placement_group}"
  tenancy                              = "${var.tenancy}"

  credit_specification {
    cpu_credits = "${var.cpu_credits}"
  }

  tags        = "${merge(map("Name", var.name), var.tags)}"
  volume_tags = "${merge(map("Name", var.name), var.volume_tags)}"

  lifecycle {
    ignore_changes = ["private_ip", "root_block_device", "ebs_block_device"]
  }
}
